﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GestionBancaria.SQLite;

namespace GestionBancaria
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * Connection SQLite
             */

            SQLiteConnection sqlite_conn;
            // Llama la clase Conection y la crea para ser llamada por la variable eh
            Connection eh = new Connection();
            sqlite_conn = eh.CreateConnection();
            eh.CreateTable(sqlite_conn);

            /**
             * Menu
             */

            int idClient = 1;
            bool reinicio = true;

            // Creación del menu de inicio
            do
            {
                Console.WriteLine("Bienvenido a tu gestión bancaria");
                Console.WriteLine("1º) Ingreso de efectivo");
                Console.WriteLine("2º) Retirada de efectivo");
                Console.WriteLine("3º) Lista de todos los movimientos");
                Console.WriteLine("4º) Lista de todos los ingresos de efectivo");
                Console.WriteLine("5º) Lista de todos las retiradas de efectivo");
                Console.WriteLine("6º) Mostrar saldo actual");
                Console.WriteLine("7º) Salir");
                Console.Write("Seleccione una opción: ");

                    switch (Console.ReadLine())
                    {
                        case "1":
                            Console.Write("¿Cuánto dinero vas a ingresar? ");
                            int Ingresar = Int32.Parse(Console.ReadLine());
                            // Envia al usuario a la function de ingresarDinero
                            eh.ingresarDinero(Ingresar, idClient);
                        break;
                        case "2":
                            Console.Write("¿Cuánto dinero vas a retirar? ");
                            int Retirar = Int32.Parse(Console.ReadLine());
                            // Envia al usuario a la function de retirarDinero
                            eh.retirarDinero(Retirar, idClient);
                        break;
                        case "3":
                            // Envia al usuario a la function de LoadTransactions
                            eh.LoadTransactions();
                        break;
                        case "4":
                            // Llama LoadIngresos
                            eh.LoadIngresos();
                        break;
                        case "5":
                            // Llama la function LoadRetiradas
                            eh.LoadRetiradas();
                        break;
                        case "6":
                            // Llama la function accountTotal
                            eh.accountTotal();
                        break;
                        case "7":
                            Console.WriteLine("Gracias por haber usado el gestor de bancos, pulse cualquier tecla para salir ...");
                            Console.ReadKey();
                            Environment.Exit(1);
                        break;
                        default:
                            Console.WriteLine("Esto no es una opción válida, por favor, escribe otra");
                        break;
                }
                Console.ReadKey();
                Console.WriteLine("Te estamos redirigiendo al menu del banco");
                Console.Clear();
            } while (reinicio == true);



            }
    }
}
