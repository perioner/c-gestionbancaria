﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GestionBancaria.SQLite
{
    class Connection
    {
        private SQLiteConnection sql_con;
        private SQLiteCommand cmd;

        /// <summary>
        /// Function para crear la conexión para SQLite
        /// </summary>
        public SQLiteConnection CreateConnection()
        {
            // Crea la conexión a la base de datos:
            sql_con = new SQLiteConnection("Data Source= database.db; Version = 3; New = True; Compress = True; ");
            
            // Abre la conexión
            try
            {
                sql_con.Open();
            }
            catch (Exception ex)
            {

            }
            return sql_con;
        }

        /// <summary>
        /// Function para crear las tablas al inicio del programa.
        /// </summary>
        public void CreateTable(SQLiteConnection conn)
        {
            string CreateBilling = "CREATE TABLE if not exists Billing (idBilling INTEGER PRIMARY KEY, " +
                "idClient INT(10), amount INT(50), extracto TEXT(5000))";
            cmd = conn.CreateCommand();
            cmd.CommandText = CreateBilling;
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Secuencia INSERT para ingresar dinero desde SQLITE
        /// </summary>
        public void ingresarDinero(int salario, int idClient)
        {
            string extracto = "Ingreso dinero en su cuenta";
            CreateConnection();
            cmd = sql_con.CreateCommand();

            cmd.CommandText = "INSERT INTO Billing (idClient, amount, extracto) VALUES ('" + idClient + "', '" + salario + "', '" + extracto + "')";
            cmd.ExecuteNonQuery();
            sql_con.Close();

            Console.WriteLine("Se han agregado " + salario + " Euros a tu cuenta");

            Console.WriteLine("Pulsa cualquier otra tecla para volver al menu de inicio");
            Console.ReadLine();
        }

        /// <summary>
        /// Secuencia INSERT para retirar dinero desde SQLITE
        /// </summary>
        public void retirarDinero(int salario, int idClient)
        {
            string extracto = "Has retirado dinero de tu cuenta";
            CreateConnection();
            cmd = sql_con.CreateCommand();

            cmd.CommandText = "INSERT INTO Billing (idClient, amount, extracto) VALUES ('" + idClient + "', '-" + salario + "', '" + extracto + "')";
            cmd.ExecuteNonQuery();
            sql_con.Close();

            Console.WriteLine("Se han retirado " + salario + " Euros de tu cuenta");

            Console.WriteLine("Pulsa cualquier otra tecla para volver al menu de inicio");
            Console.ReadLine();
        }

        /// <summary>
        /// Secuencia SELECT para cargar todos los ingresos desde SQLITE
        /// </summary>
        public void LoadIngresos()
        {
            CreateConnection();
            string selectSQL = "SELECT * FROM Billing WHERE idClient = 1 and amount >= 0";
            var cmd = new SQLiteCommand(selectSQL, sql_con);
            SQLiteDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                Console.WriteLine("Numero de cliente: " + $"{rdr.GetInt32(1)}");
                Console.WriteLine("Transación realizada: " + $"{rdr.GetString(3)}");
                Console.WriteLine("Total en trasación: " + $"{rdr.GetInt32(2)}" + " euros ");
            }

            Console.ReadKey();
            sql_con.Close();
        }

        /// <summary>
        /// Secuencia SELECT para cargar todas las retiradas desde SQLITE
        /// </summary>
        public void LoadRetiradas()
        {
            CreateConnection();
            string selectSQL = "SELECT * FROM Billing WHERE idClient = 1 and amount <= 0";
            var cmd = new SQLiteCommand(selectSQL, sql_con);
            SQLiteDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                Console.WriteLine("Numero de cliente: " + $"{rdr.GetInt32(1)}");
                Console.WriteLine("Transación realizada: " + $"{rdr.GetString(3)}");
                Console.WriteLine("Total en trasación: " + $"{rdr.GetInt32(2)}" + " euros ");
            }

            Console.ReadKey();
            sql_con.Close();
        }

        /// <summary>
        /// Secuencia SELECT para cargar todas las transaciones desde SQLITE
        /// </summary>
        public void LoadTransactions()
        {
            CreateConnection();
            string selectSQL = "SELECT * FROM Billing WHERE idClient = 1";
            var cmd = new SQLiteCommand(selectSQL, sql_con);
            SQLiteDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                Console.WriteLine("Numero de cliente: " + $"{rdr.GetInt32(1)}");
                Console.WriteLine("Transación realizada: " + $"{rdr.GetString(3)}");
                Console.WriteLine("Total en trasación: " + $"{rdr.GetInt32(2)}" + " euros ");
            }

            Console.ReadKey();
            sql_con.Close();
        }

        /// <summary>
        /// Secuencia SELECT que suma todas las cantidades de la columna amount
        /// </summary>
        public void accountTotal()
        {
            CreateConnection();
            string selectSQL = "SELECT SUM(amount) FROM Billing WHERE idClient = 1";
            var cmd = new SQLiteCommand(selectSQL, sql_con);
            SQLiteDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                Console.WriteLine("Su saldo actual es de " + $"{rdr.GetInt32(0)}" + " euros en su cuenta bancaria");
            }

            Console.ReadKey();
            sql_con.Close();
        }
    }
}
